#include <iostream>
#include "process.h"
#include "processor.h"
#include "machine.h"
#include "channel_set.h"
#include "channel.h"
#include "array.h"
#include "timer_t.h"

using namespace std;

class timer_example_t : public process_t {

public:
	void behave() {
		
		timer_t t;
		
		t.reader(processor);
		
		cout << "Sleeping ...";
		
		t.wait_until(t.read() + 2000);
		
		cout << "... Woke up!";
		}
};

/*

size of various data types:

channel: 24 bytes
process: 32 bytes
jmp_buf (Win32): 16*4 = 64 bytes
processor: 24 + 16*4 = 88 bytes
machine: 88 * NUM_PROCESSORS + 32

*/

int main() {

	machine_t machine;

	timer_example_t example;

	machine.run(&example);

	while (true);

}

