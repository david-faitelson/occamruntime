#pragma once
class altable_t
{
public:
	virtual void expect_to_read() { }

	virtual void cancel_read() { }

	virtual bool is_ready_to_read() {
		return false;
	}
};

