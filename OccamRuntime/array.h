#pragma once

template<class T, int N>
class array_t
{
private:
	T element[N];
public:
	T& at(int i) { return element[i]; }
	int size() { return N;  }
};

