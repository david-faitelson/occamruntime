#pragma once
#include "channel_base.h"
#include <assert.h>
#include "channel.h"
#include "processor.h"

template<class T>
class channel_t :
	public channel_base_t
{
private:
	T content;

public:

	T read() {

		assert(reader_ && reader_->is_running());

		isExpected = true;

		if (isReady) { // there is a writer waiting for us, release it
			writer_->make_ready();
		}
		else { // there's no writer, wait for one 
			reader_->make_waiting();
			reader_->yield_to_scheduler_processor();
		}

		assert(isReady && isExpected);

		isReady = false; isExpected = false;
		return content;
	}

	void write(T value) {

		assert(writer_ && writer_->is_running());

		// don't overwrite the content until the reader has had a chance to
		// read the previous value.

		while (isReady)
			writer_->yield_to_scheduler_processor();

		isReady = true;
		content = value;

		if (isExpected) { // there's a reader waiting for us, release it and let it read the content immediately
			reader_->make_ready();
			writer_->yield_to_scheduler_processor();
		}
		else { // there's no reader, wait for one.
			writer_->make_waiting();
			writer_->yield_to_scheduler_processor();
		}
	}
};

