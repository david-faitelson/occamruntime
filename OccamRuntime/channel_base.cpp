#include "channel_base.h"
#include "processor.h"

void channel_base_t::expect_to_read() {

	assert(reader_ && reader_->is_running());

	assert(isExpected == false);

	isExpected = true;
}

void channel_base_t::cancel_read() {

	assert(reader_ && reader_->is_running());

	isExpected = false;

}

bool channel_base_t::is_ready_to_read() {

	return isReady;
}

