#pragma once
#include <assert.h>
#include "altable_t.h"

class processor_t;

class channel_base_t :public altable_t {

protected:
	processor_t* reader_;
	processor_t* writer_;
	bool isReady;
	bool isExpected;

	channel_base_t* next_;

public:

	channel_base_t() {
		reader_ = 0;
		writer_ = 0;
		isReady = false;
		isExpected = false;

		next_ = 0;
	}

	void writer(processor_t* p) {
		writer_ = p;
	}

	void reader(processor_t* p) {
		reader_ = p;
	}

	void expect_to_read();

	void cancel_read();

	bool is_ready_to_read();

	void next(channel_base_t* c) { next_ = c;  }

	channel_base_t* next() { return next_;  }
};
