#include "channel_set.h"
#include "channel_base.h"
#include "process.h"

channel_base_t* channel_set_t::select_for(process_t* process) {
	
	// indicate that we are willing to read from all the channels in the set.

	channel_base_t* next = first;

	while (next != 0) {
		next->expect_to_read();
		next = next->next();
	}

	// find a channel that is ready (has data)

	next = first;

	while (next != 0 && !next->is_ready_to_read()) {
		next = next->next();
	}

	// while no such channel exists ...

	while (next == 0) {

		// put this process to sleep (it will be awaken when one of the channels it reads from
		// will have ready data --- but not necessarily a channel in this channel set, that's why
		// we have the loop.

		process->wait();

		// see if now there's a channel ready to read.

		next = first;
		while (next != 0 && !next->is_ready_to_read()) {
			next = next->next();
		}
	}

	// we have found a channel with data.

	channel_base_t* ready = next;

	// we no longer wish to read from the other channels in the set.

	next = first;

	while (next != 0) {
		next->cancel_read();
		next = next->next();
	}

	// return the channel that has the data.

	return ready;
}
