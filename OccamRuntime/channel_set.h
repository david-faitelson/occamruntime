#pragma once

class channel_base_t;
class process_t;

class channel_set_t
{
private:
	channel_base_t* first;
public:
	channel_set_t(channel_base_t* c) { first = c;  }

	channel_base_t* select_for(process_t*);

};

