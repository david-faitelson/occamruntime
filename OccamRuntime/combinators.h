#pragma once

#include "process.h"

class skip_t : public process_t {

public:

	void behave() {
		;
	}
};

class stop_t : public process_t {

public:

	void behave() {
		while (true)
			yield();
	}
};

class seq_t : public process_t {

private:
	process_t* first;

public:

	seq_t(process_t* f) {
		first = f;
	}

	void behave() {

		process_t* next = first;
		while (next != 0) {
			next->fork();
			join(next);
			next = next->next();
		}
	}
};


class par_t : public process_t {

private:
	process_t* first;

public:

	par_t(process_t* f) {
		first = f;
	}

	void behave() {

		process_t* next = first;
		while (next != 0) {
			next->fork(); 
			next = next->next();
		}

		next = first;

		while (next != 0) {
			join(next);
			next = next->next();
		}

	}
};

stop_t stop() {
	return stop_t();
}

skip_t skip() {
	return skip_t();
}
