#include <assert.h>
#include "machine.h"

volatile char* machine_t::stackp ATTRIBUTE((used)) /* __attribute__((used))*/;

machine_t* machine_t::the_machine = 0;

machine_t::machine_t() {

	assert(the_machine == 0); // there must be exactly one machine in the system
	the_machine = this; 
}

machine_t* machine_t::machine() { return the_machine;  }

void machine_t::run(process_t* p) {

	place_scheduler_on(&processor[0]);
	p->place_on(&processor[1]);

	int i = 0;
	while (i < NUM_PROCESSORS) {
		if (setjmp(processor[i].context) == 0) {

			/* the first time we get here we use alloca to move the stack pointer
			and thus make a free space for the stack of processor[i]. */

			stackp = (char*)alloca(OCCAM_STACK_SIZE);
			i++;
		}
		else {
			/* the next time we get here is when the scheduler processor makes a longjump
			to the state of a processor for the first time. in this case must start the
			processor loop but we don't know which processor to start because we could be
			in any of the machine processors. */

			scheduler.get_running_processor()->start();
		}
	}

	scheduler.set_running_processor(&processor[0]);
	longjmp(processor[0].context, 1);
}

void machine_t::place_scheduler_on(processor_t* p) {
	scheduler.place_on(p);
}

processor_t* machine_t::processor_at(int index) { return &processor[index]; }

processor_t* machine_t::allocate_processor() {
	processor_t* p = 0;
	int i = 0;
	while (i < NUM_PROCESSORS && !processor[i].is_idle())
		i++;
	if (i == NUM_PROCESSORS)
		return 0;
	return &processor[i];
}
