#pragma once
#include "processor.h"
#include "scheduler.h"
#include "platform.h"

class machine_t {
private:
	processor_t processor[NUM_PROCESSORS];
	scheduler_t scheduler;

	volatile static char* stackp /*__attribute__((used))*/;


	static machine_t* the_machine;

public:

	static machine_t* machine();

public:
	machine_t();

	void run(process_t*);

	void place_scheduler_on(processor_t* p);

	processor_t* allocate_processor();

	processor_t* processor_at(int index);
};
