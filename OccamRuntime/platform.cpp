#ifdef _MSC_VER

#include <sys/timeb.h>

__time64_t millis() {
	struct _timeb timebuffer;
	_ftime64_s(&timebuffer);
	return timebuffer.millitm + 1000 * timebuffer.time; 
}

bool after(__time64_t x, __time64_t y) {
	return x - y > 0;
}

#endif

#ifdef __AVR__

bool after(__time64_t x, __time64_t y) {
	return x - y < y - x;
}

#endif 