#pragma once

#ifdef __AVR__

#define NUM_PROCESSORS 10
#define OCCAM_STACK_SIZE 256

#define ATTRIBUTE(a) __attribute__((a))

#define __time64_t unsigned long

#include<alloca.h>

#endif

#ifdef _MSC_VER

#define NUM_PROCESSORS 10
#define OCCAM_STACK_SIZE 8192

#define ATTRIBUTE(a) 

#include <malloc.h>

__time64_t millis();

#endif

bool after(__time64_t x, __time64_t y);

