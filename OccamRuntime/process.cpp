#include <assert.h>
#include "process.h"
#include "processor.h"
#include "machine.h"
#include "platform.h"

void process_t::place_on(processor_t* p) {

	assert(processor == 0);

	assert(p->is_idle());

	p->assign_process(this);
	processor = p;
}

void process_t::yield() {
	processor->yield_to_scheduler_processor();
}

void process_t::wait() {
	processor->make_waiting();
	yield();
}

channel_t<bool>* process_t::termination_channel() { return &term_; }

void process_t::fork() {
	place_on(machine_t::machine()->allocate_processor());
}

void process_t::join(process_t* child) {
	child->termination_channel()->reader(processor); // make my processor the reader of the child's termination channel.
	child->termination_channel()->read();
}


