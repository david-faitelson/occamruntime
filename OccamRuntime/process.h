#pragma once
#include <assert.h>
#include "channel.h"

class machine_t;

class processor_t;

class process_t {

	friend machine_t;
	friend processor_t;

protected:

	processor_t* processor; // the processor that runs this process.

	channel_t<bool> term_; // termination channel for this process.

public:

	process_t() { processor = 0; }

	virtual void behave() = 0;

	void fork();

	void yield();

	void wait();

	void join(process_t* child);

private:
	void place_on(processor_t* p);

	channel_t<bool>* termination_channel();

};
