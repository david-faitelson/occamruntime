#include "processor.h"
#include "channel.h"
#include "process.h"
#include "machine.h"
#include "platform.h"

processor_t::processor_t() {

	stat = IDLE;
}

void processor_t::start() {

	while (true) {
		if (stat != IDLE) {
			process->termination_channel()->writer(this);
			process->behave();
			process->termination_channel()->write(0);
			process = 0;
			stat = IDLE;
		}
		else
			yield_to_scheduler_processor();
	}
}

void processor_t::yield_to(processor_t* other) {

	if (setjmp(context) == 0) {
		longjmp(other->context, 1);
	}
}

void processor_t::yield_to_scheduler_processor() {
	yield_to(machine_t::machine()->processor_at(0));
}

void processor_t::make_ready() {
	stat = READY;
}

void processor_t::make_waiting() {
	stat = WAITING;
}

void processor_t::make_running() {
	stat = RUNNING;
}

void processor_t::assign_process(process_t* p) {
	assert(stat = IDLE);
	process = p;
	stat = READY;
}

processor_t::status_t processor_t::get_status() { return stat; }

bool processor_t::is_running() { return stat == RUNNING; }

bool processor_t::is_ready() { return stat == READY; }

bool processor_t::is_idle() { return stat == IDLE; }
