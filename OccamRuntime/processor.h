#pragma once

#include<setjmp.h>

class process_t;
class machine_t;

class processor_t {

public:

	enum status_t { RUNNING, READY, WAITING, IDLE };

	processor_t();

	void start();

	void yield_to(processor_t* other);

	void yield_to_scheduler_processor();

	void make_ready();

	void make_waiting();

	void make_running();

	void assign_process(process_t* p);

	status_t get_status();

	bool is_running();

	bool is_ready();

	bool is_idle();

	friend machine_t;

private:
	status_t stat;

	jmp_buf context;

	process_t* process;
};
