#include "machine.h"
#include "scheduler.h"

void scheduler_t::behave() {

	int next = 1; // 0 is the scheduler processor

	while (true) {

		processor_t* p = machine_t::machine()->processor_at(next);

		if (p->is_ready()) {

			p->make_running();

			running_processor = p;

			processor->yield_to(p);

			if (p->is_running())
				p->make_ready();

		}
		next = (next + 1) % NUM_PROCESSORS;
	}
}
