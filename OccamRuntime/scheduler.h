#pragma once
#include "process.h"

class processor_t;

class scheduler_t : public process_t {

private: 
	processor_t* running_processor;

public:

	virtual void behave();

	void set_running_processor(processor_t* p) { running_processor = p; }

	processor_t* get_running_processor() { return running_processor;  }
};
