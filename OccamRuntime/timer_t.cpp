#include "platform.h"
#include "timer_t.h"
#include "processor.h"
#include "assert.h"

__time64_t timer_t::read()
{
	//assert(is_ready_to_read());
	return millis();
}

void timer_t::wait_until(__time64_t t) {

	while (!after(millis(), t))
		reader_->yield_to_scheduler_processor();

}
