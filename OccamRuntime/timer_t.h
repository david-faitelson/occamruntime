#pragma once
#include "altable_t.h"

class processor_t;

class timer_t :
	public altable_t
{
private:
	__time64_t time_ready;
	processor_t* reader_;
public:

	timer_t() { time_ready = millis(); }

	bool is_ready_to_read() { return after(millis(), time_ready); }

	void reader(processor_t* p) { reader_ = p;  }

	void expect_to_read_at(__time64_t t) { time_ready = t;  }

	void wait_until(__time64_t t);

	__time64_t read();

};

